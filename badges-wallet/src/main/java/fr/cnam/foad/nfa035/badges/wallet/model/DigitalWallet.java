package fr.cnam.foad.nfa035.badges.wallet.model;



import java.util.Set;

public class DigitalWallet {

    Set<DigitalBadge> allBadges;
    Set<Long> deadLinesPositions;
    Set<DigitalBadge> deletingBadges;


    public DigitalWallet(Set<DigitalBadge> allBadges, Set<Long> deadLinesPositions, Set<DigitalBadge> deletingBadges) {
        this.allBadges = allBadges;
        this.deadLinesPositions = deadLinesPositions;
        this.deletingBadges = deletingBadges;
    }

    public void addDeletingBadge(DigitalBadge digitalBadge){
        this.deletingBadges.add(digitalBadge);

    }

    public void commitBadgeDeletion(){

    }

    public Set<DigitalBadge> getAllBadges() {
        return allBadges;
    }

    public void setAllBadges(Set<DigitalBadge> allBadges) {
        this.allBadges = allBadges;
    }

    public Set<Long> getDeadLinesPositions() {
        return deadLinesPositions;
    }

    public void setDeadLinesPositions(Set<Long> deadLinesPositions) {
        this.deadLinesPositions = deadLinesPositions;
    }

    public Set<DigitalBadge> getDeletingBadges() {
        return deletingBadges;
    }

    public void setDeletingBadges(Set<DigitalBadge> deletingBadges) {
        this.deletingBadges = deletingBadges;
    }
}
